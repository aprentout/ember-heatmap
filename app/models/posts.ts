import Model, { attr } from '@ember-data/model';

export default class PostsModel extends Model {
  @attr()
  declare data: number[];

  @attr('number', { defaultValue: 0 })
  declare counter: number;
}
