import Service from '@ember/service';
import { service } from '@ember/service';

import Store from '@ember-data/store';

const decoder = new TextDecoder();
const ByteType = 'data: ';

// Handle multiple key type
interface OpenObject {
  [key: string]: any;
}

const keyType = [
  'pin',
  'instagram_media',
  'youtube_video',
  'article',
  'tweet',
  'facebook_status',
];

export default class StreamService extends Service {
  @service declare store: Store;

  constructor(...args: any) {
    super(...args);

    this.initModels();

    this.initStream();
  }

  get data() {
    return this.store?.peekAll('posts');
  }

  // Retreive data from the stream
  initStream(): void {
    fetch('https://stream.upfluence.co/stream')
      .then((response) => response.body)
      .then((body) => {
        if (body) {
          const self = this;
          const reader = body.getReader();

          reader
            .read()
            //Reader
            .then(async function processText({ done, value }): Promise<any> {
              if (done) {
                console.log('Stream complete');
                return;
              }

              const decoded = self.decode(value);

              if (decoded) {
                await keyType.forEach(async (type) => {
                  if (decoded[type]) {
                    let data = self.store.peekRecord('posts', type);
                    data.data.push(decoded[type].timestamp);
                    // force refresh
                    data.data = [...data.data];
                    data.counter++;
                  }
                });
              }

              if (value) return reader.read().then(processText);
            });
        }
      });
  }

  // Return decoded object if Uint8Array match ByteType
  decode = (buffer: Uint8Array): OpenObject | undefined => {
    const value = decoder.decode(buffer);
    if (value.startsWith(ByteType)) {
      try {
        return JSON.parse(value.slice(ByteType.length));
      } catch (err) {
        return;
      }
    }
  };

  // Init posts in ember store with id based on keyType
  initModels = (): void => {
    keyType.forEach((type) => {
      this.store.push({
        data: [
          {
            id: type,
            type: 'posts',
            attributes: {
              data: [],
              counter: 0,
            },
          },
        ],
      });
    });
  };
}
