import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { service } from '@ember/service';

import StreamService from '../services/stream';

interface Args {
  count: number;
  max: number;
  hour: number;
  day: number;
}

export default class HeatpointComponent extends Component<Args> {
  @service declare stream: StreamService;

  get day() {
    return this.args?.day;
  }

  get hour() {
    return this.args?.hour;
  }

  get data() {
    return this.args.count;
  }

  get max() {
    return this.args.max || 0;
  }

  // divide by max to adapt size
  get percent() {
    if (this.data == 0) {
      return '0%';
    }

    if (this.max > 0) {
      let ratio = Math.ceil((this.data / this.max) * 100);

      if (ratio < 5) {
        ratio = 5;
      }
      return ratio + '%';
    }

    return '';
  }

  get style() {
    return htmlSafe(`height: ${this.percent}; width: ${this.percent};`);
  }
}
