import Component from '@glimmer/component';
import { service } from '@ember/service';

import { A } from '@ember/array';

import StreamService from 'ember-heatmap/services/stream';
import { HOURS } from 'ember-heatmap/consts';

interface Args {
  data: any;
}

// return UTC hour from timestamp
const getHour = (date: number): number => {
  return new Date(date).getUTCHours();
};

// return UTC week index from timestamp
const getDayOfWeek = (date: number): number => {
  return new Date(date).getUTCDay();
};

export default class HeatmapComponent extends Component<Args> {
  @service declare stream: StreamService;

  get data() {
    let arr: number[][] = A([]);

    for (let i = 0; i < 7; i++) {
      arr.pushObject(A(new Array(24).fill(0)));
    }

    if (this.args?.data) {
      this.args?.data.data.forEach((timestamp: number) => {
        const day = getDayOfWeek(timestamp);
        const hour = getHour(timestamp);

        const val = arr.objectAt(day)?.objectAt(hour) || 0;
        arr.objectAt(day)?.set(hour, val + 1);
      });
    }

    return arr;
  }

  get max() {
    let arr: number[] = [];

    this.data.forEach((data: number[]) => {
      arr = [...arr, ...data];
    });

    arr.sort();
    return Math.max(...arr);
  }

  get total() {
    return this.args.data.counter;
  }

  get yAxis() {
    return HOURS;
  }
}
