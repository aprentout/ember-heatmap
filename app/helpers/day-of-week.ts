import { helper } from '@ember/component/helper';
import { DAYS } from 'ember-heatmap/consts';

export default helper(function dayOfWeek([dayIndex] /*, named*/) {
  if (typeof dayIndex === 'string') {
    dayIndex = parseInt(dayIndex);
  }
  if (typeof dayIndex === 'number') {
    if (dayIndex >= 0 && dayIndex < DAYS.length) {
      return DAYS[dayIndex];
    }
  }
  return null;
});
