import Controller from '@ember/controller';
import { service } from '@ember/service';

import StreamService from '../services/stream';

export default class ApplicationController extends Controller {
  @service declare stream: StreamService;

  constructor(...args: any) {
    super(...args);

    this.stream.initStream();
  }
}
