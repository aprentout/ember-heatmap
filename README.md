# ember-heatmap

This README outlines the details of collaborating on this Ember application.
A short introduction of this app could easily go here.

## Objective
Create a front-end application that draws a 3-dimensional visualization for each type of social posts.

The 3 dimensions should be:

- Day of the week
- Hour of the day (UTC time)
- Number of post matching the two other dimensions

Data came from https://stream.upfluence.co/stream (HTTP API stream)

## Choice
- Initialy start this project using vue.js 3, decided to use ember.js to match front stack
- Data is handled asynchronously in a service 
  - Allow to handle stream in a unique point
  - Data can be accessed in the entire app if required
 - Create a "post" model to handle stream data in the ember store
 - Create 2 components
	 - Heatmap: In charge of transform timestamp array into 2 dimensions array based on weekday and hour of the post
	 - Heatpoint: In charge to display the data as a circle

## What can be improve ?
  - Batch insertion
  - Read data every x seconds
  - Improve punch size by using a true maths formula
  - Better Ux with transition :)
  - Add some tests for service stream

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/) (with npm)
* [Ember CLI](https://cli.emberjs.com/release/)
* [Google Chrome](https://google.com/chrome/)

## Installation

* `git clone <repository-url>` this repository
* `cd ember-heatmap`
* `npm install`

## Running / Development

* `ember serve`
* Visit your app at [http://localhost:4200](http://localhost:4200).
### Running Tests

* `ember test`