import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-heatmap/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Helper | dayOfWeek', function (hooks) {
  setupRenderingTest(hooks);

  // TODO: Replace this with your real tests.
  test('it renders', async function (assert) {
    this.set('inputValue', '1234');

    await render(hbs`{{day-of-week this.inputValue}}`);

    assert.dom(this.element).hasText('');

    this.set('inputValue', 0);
    assert.dom(this.element).hasText('dim');

    this.set('inputValue', 1);
    assert.dom(this.element).hasText('lun');

    this.set('inputValue', 2);
    assert.dom(this.element).hasText('mar');

    this.set('inputValue', 3);
    assert.dom(this.element).hasText('mer');

    this.set('inputValue', 4);
    assert.dom(this.element).hasText('jeu');

    this.set('inputValue', 5);
    assert.dom(this.element).hasText('ven');

    this.set('inputValue', 6);
    assert.dom(this.element).hasText('sam');

    this.set('inputValue', 7);
    assert.dom(this.element).hasText('');
  });
});
