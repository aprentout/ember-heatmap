import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-heatmap/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

import { HOURS } from 'ember-heatmap/consts';

module('Integration | Component | heatmap', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<Heatmap />`);

    assert.dom('tbody').exists();
    assert.dom('tfoot').hasText(HOURS.join(' '));
  });
});
