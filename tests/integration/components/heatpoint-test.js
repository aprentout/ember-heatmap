import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-heatmap/tests/helpers';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | heatpoint', function (hooks) {
  setupRenderingTest(hooks);

  test('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<Heatpoint @count="22" @max="22" />`);

    assert.dom('div').exists();
    assert.dom('div').hasClass('w-full');
    assert.dom('div').hasClass('h-full');
    assert.dom('div').hasClass('flex');
    assert.dom('div').hasClass('justify-center');
    assert.dom('div').hasClass('items-center');

    assert.dom('.bg-black').exists();
  });
});
